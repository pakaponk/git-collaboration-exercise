import React from 'react';
import './App.css';
import students from './student.mock';
import {
  getRandomStudent,
  getMinScore,
  getMaxScore,
  getStandardDeviation,
  getStudentRank,
  getAverageScore,
  getScoreFrequencies,
} from './utils';

const SCORE_COLUMNS = 10;
const MAX_SCORE = 100;
const SCORE_RANGE = MAX_SCORE / SCORE_COLUMNS;

function App() {
  const student = getRandomStudent(students);
  const { name, score } = student;
  const min = getMinScore(students);
  const max = getMaxScore(students);
  const avg = getAverageScore(students);
  const std = getStandardDeviation(students);
  const rank = getStudentRank(students, student);
  const frequencies = getScoreFrequencies(students, SCORE_RANGE, SCORE_COLUMNS);

  return (
    <div className="App">
      <header className="App-header">
        <h2>Hi, {name}</h2>
        <p>
          On your last exam, you are rank <b>{rank}th</b> out of <b>100</b>.
          <br />
          Your score is {score}
        </p>
        <h3>Here is the exam statistic</h3>
        <div style={{ display: 'flex', width: '360px', justifyContent: 'space-between' }}>
          <div>MIN: {min}</div>
          <div>MAX: {max}</div>
          <div>AVG: {avg}</div>
          <div>STD: {std}</div>
        </div>
        <h3>Score distribution</h3>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          {Array.from({ length: SCORE_COLUMNS }).map((_, index) => (
            <div key={index} style={{ padding: 20 }}>
              <div>{`${SCORE_RANGE * index} - ${SCORE_RANGE * (index + 1)}`}</div>
              <div>{frequencies[index]}</div>
            </div>
          ))}
        </div>
      </header>
    </div>
  );
}

export default App;
