/**
 * @typedef Student
 * @property {String} name Student's name
 * @property {Number} score Student's score
 */

/**
 * Randomly return one of the given `students`
 * @param {Student[]} students
 */
export function getRandomStudent(students) {
  const randomIndex = Math.ceil(Math.random() * students.length) - 1;
  return students[randomIndex];
}

/**
 * Please implement a function that you was assigned to
 * In this file
 */
export function getMinScore(students) {
  return 0;
}

export function getMaxScore(students) {
  let score = students.map(function(obj) {
    return obj.score;
  });
  return score.reduce(function(previousScore, currentScore) {
    return previousScore > currentScore ? previousScore : currentScore;
  }, 0);
}

export function getAverageScore(students) {
  const sum = students.reduce((total, currentValue) => total + currentValue.score, 0);
  const avg = sum / students.length;
  return avg;
}

export function getStandardDeviation(students) {
  return 10;
}

export function getStudentRank(students, currentStudent) {
  students.sort((a, b) => (a.score > b.score ? 1 : -1));
  const index = students.findIndex(student => student.score === currentStudent.score);
  return index + 1;
}

export function getScoreFrequencies(students, scoreRange, totalColumn) {
  let stdCountByScoreRange = [];
  if (students.length) {
    let score = 0;
    let round = 0;
    let stdInfoInRange = [];
    while (round < totalColumn) {
      let min = score;
      let max = min + scoreRange;

      if (round === 0) {
        scoreRange = scoreRange - 1;
      }

      stdInfoInRange = students.filter(std => {
        return std.score >= min && std.score <= max;
      });

      stdCountByScoreRange.push(stdInfoInRange.length);
      score = max + 1;
      round++;
    }
  }
  return stdCountByScoreRange;
}
