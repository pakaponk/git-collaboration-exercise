const students = [
  {
    name: 'Shafira Fitzgerald',
    score: 88,
  },
  {
    name: 'Amir Bowers',
    score: 83,
  },
  {
    name: 'Beverly Farley',
    score: 19,
  },
  {
    name: 'Nicholas Buck',
    score: 67,
  },
  {
    name: 'Jael Hays',
    score: 49,
  },
  {
    name: 'Kasimir Key',
    score: 13,
  },
  {
    name: 'Illiana Barron',
    score: 92,
  },
  {
    name: 'Jack Palmer',
    score: 23,
  },
  {
    name: 'Jonas Pitts',
    score: 6,
  },
  {
    name: 'Karina Fulton',
    score: 23,
  },
  {
    name: 'Lewis Herring',
    score: 67,
  },
  {
    name: 'Henry Garrison',
    score: 82,
  },
  {
    name: 'Jacqueline Richards',
    score: 59,
  },
  {
    name: 'Chanda Sawyer',
    score: 39,
  },
  {
    name: 'Cynthia Vazquez',
    score: 31,
  },
  {
    name: 'Lionel Burt',
    score: 14,
  },
  {
    name: 'Kalia Rodriguez',
    score: 0,
  },
  {
    name: 'Tanya Hogan',
    score: 88,
  },
  {
    name: 'Francesca Dyer',
    score: 74,
  },
  {
    name: 'Chastity Mcneil',
    score: 100,
  },
  {
    name: 'Basil Colon',
    score: 32,
  },
  {
    name: 'Ignatius Baxter',
    score: 20,
  },
  {
    name: 'Leilani Wheeler',
    score: 6,
  },
  {
    name: 'Avye Mayo',
    score: 34,
  },
  {
    name: 'Rina Gonzalez',
    score: 96,
  },
  {
    name: 'Linda Hardin',
    score: 81,
  },
  {
    name: 'Damon Baird',
    score: 5,
  },
  {
    name: 'Melodie Whitley',
    score: 0,
  },
  {
    name: 'Nicholas Vaughan',
    score: 71,
  },
  {
    name: 'Gray Rutledge',
    score: 77,
  },
  {
    name: 'Taylor Quinn',
    score: 1,
  },
  {
    name: 'Colt Washington',
    score: 13,
  },
  {
    name: 'Mollie Bond',
    score: 18,
  },
  {
    name: 'Kennedy Wells',
    score: 27,
  },
  {
    name: 'Shaeleigh Medina',
    score: 22,
  },
  {
    name: 'Maia Horn',
    score: 30,
  },
  {
    name: 'Cheryl Stanton',
    score: 84,
  },
  {
    name: 'Sade Kramer',
    score: 8,
  },
  {
    name: 'Ariel Bell',
    score: 7,
  },
  {
    name: 'Shoshana Cervantes',
    score: 44,
  },
  {
    name: 'Lunea Alexander',
    score: 39,
  },
  {
    name: 'Imogene Oneil',
    score: 53,
  },
  {
    name: 'Garrett Vinson',
    score: 1,
  },
  {
    name: 'Kirk Hughes',
    score: 91,
  },
  {
    name: 'Bertha Edwards',
    score: 66,
  },
  {
    name: 'Desiree Kinney',
    score: 18,
  },
  {
    name: 'Hanae Salas',
    score: 54,
  },
  {
    name: 'Kendall Keller',
    score: 57,
  },
  {
    name: 'Reuben Travis',
    score: 4,
  },
  {
    name: 'Len May',
    score: 48,
  },
  {
    name: 'Alexander Smith',
    score: 88,
  },
  {
    name: 'Sebastian Waller',
    score: 90,
  },
  {
    name: 'Orlando Noble',
    score: 3,
  },
  {
    name: 'Nina Blankenship',
    score: 42,
  },
  {
    name: 'Irene Ellison',
    score: 92,
  },
  {
    name: 'Jakeem Mcbride',
    score: 81,
  },
  {
    name: 'Melvin Banks',
    score: 25,
  },
  {
    name: 'Kyla Mason',
    score: 37,
  },
  {
    name: 'Darius Odom',
    score: 5,
  },
  {
    name: 'Evan Sosa',
    score: 23,
  },
  {
    name: 'Jorden Hatfield',
    score: 77,
  },
  {
    name: 'Kristen Bird',
    score: 38,
  },
  {
    name: 'Tyler Rivera',
    score: 60,
  },
  {
    name: 'MacKensie Kent',
    score: 41,
  },
  {
    name: 'Pascale Mosley',
    score: 98,
  },
  {
    name: 'Kyla Ray',
    score: 22,
  },
  {
    name: 'William Curry',
    score: 25,
  },
  {
    name: 'Byron Franklin',
    score: 41,
  },
  {
    name: 'Andrew Cooley',
    score: 20,
  },
  {
    name: 'Ingrid Barrera',
    score: 28,
  },
  {
    name: 'Kato Glass',
    score: 71,
  },
  {
    name: 'MacKensie Hurley',
    score: 48,
  },
  {
    name: 'Scarlet Vincent',
    score: 92,
  },
  {
    name: 'Teegan Rose',
    score: 73,
  },
  {
    name: 'Alexandra Pollard',
    score: 63,
  },
  {
    name: 'Yuri Pearson',
    score: 20,
  },
  {
    name: 'Nathan Mason',
    score: 67,
  },
  {
    name: 'Gillian Salinas',
    score: 61,
  },
  {
    name: 'Hakeem Koch',
    score: 76,
  },
  {
    name: 'Sheila Pena',
    score: 14,
  },
  {
    name: 'Timon Robertson',
    score: 87,
  },
  {
    name: 'Dylan Hughes',
    score: 83,
  },
  {
    name: 'Alexandra Allen',
    score: 61,
  },
  {
    name: 'Jennifer Maddox',
    score: 30,
  },
  {
    name: 'Ivory White',
    score: 88,
  },
  {
    name: 'Jenette Howell',
    score: 65,
  },
  {
    name: 'Imelda Cooper',
    score: 71,
  },
  {
    name: 'Rae King',
    score: 93,
  },
  {
    name: 'Ulla Reyes',
    score: 55,
  },
  {
    name: 'Leilani Hammond',
    score: 45,
  },
  {
    name: 'Vincent Morse',
    score: 90,
  },
  {
    name: 'Tiger Alvarez',
    score: 54,
  },
  {
    name: 'Hamilton Snider',
    score: 49,
  },
  {
    name: 'Sarah Mathis',
    score: 19,
  },
  {
    name: 'Jenette Levine',
    score: 60,
  },
  {
    name: 'Steven Oliver',
    score: 92,
  },
  {
    name: 'Montana Byers',
    score: 2,
  },
  {
    name: 'Orla Montoya',
    score: 68,
  },
  {
    name: 'Thaddeus Hubbard',
    score: 19,
  },
  {
    name: 'Wesley Brown',
    score: 44,
  },
];

export default students;
